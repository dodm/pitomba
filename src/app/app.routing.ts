import {RouterModule, Routes} from '@angular/router';
import {NgModule} from '@angular/core';


const appRoutes: Routes = [
    { path: '', redirectTo: '/', pathMatch: 'full'},
    { path: 'produto', loadChildren: './dominio/produto/produto.module#ProdutoModule'},
    { path: 'carrinho', loadChildren: './dominio/carrinho/carrinho.module#CarrinhoModule' },
    { path: 'categoria', loadChildren: './dominio/categoria/categoria.module#CategotiaModule'},
    { path: 'promocao', loadChildren: './dominio/promocao/promocao.module#PromocaoModule' }
];

@NgModule({
    imports: [RouterModule.forRoot(
        appRoutes,
        { enableTracing: true }
    )],
    exports: [RouterModule]
  })

  export class AppRouting {}