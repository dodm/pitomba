import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { RouterModule } from '@angular/router';

import { AppComponent } from './app.component';

import { AlertModule } from 'ngx-bootstrap';
import { AppRouting } from './app.routing';
import { HomeComponent } from './dominio/home/home.component';
import { CarrinhoModule } from './dominio/carrinho/carrinho.module';
import { CarrinhoService } from './dominio/carrinho/carrinho.service';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent
  ],
  imports: [
    BrowserModule,
    RouterModule,
    HttpClientModule,
    AlertModule.forRoot(),
    CarrinhoModule,

    // app
    AppRouting
  ],
  providers: [
    CarrinhoService
  ],
  bootstrap: [AppComponent],
})
export class AppModule { }
