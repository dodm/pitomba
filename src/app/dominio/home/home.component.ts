import { Component, OnInit } from '@angular/core';
import { CarrinhoService } from '../carrinho/carrinho.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  constructor(private carrinhoService:CarrinhoService){}
  totalQuantidade:number;
  
  ngOnInit() {
    this.totalQuantidade = this.carrinhoService.totalQuantidade; 
    window.setInterval(() => {this.totalQuantidade = this.carrinhoService.totalQuantidade},500);
  }

  // atualizaCarrinho(event) {
  //   console.log("fui chamado",event);
    
  //   this.totalQuantidade = event;
  // }

  atualizaCarrinho() {
    console.log("fui chamado");
    
    this.totalQuantidade = this.carrinhoService.totalQuantidade; 
  }
  up() {

  }

}
