import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PromocaoCriarComponent } from './promocao-criar.component';

describe('PromocaoCriarComponent', () => {
  let component: PromocaoCriarComponent;
  let fixture: ComponentFixture<PromocaoCriarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PromocaoCriarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PromocaoCriarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
