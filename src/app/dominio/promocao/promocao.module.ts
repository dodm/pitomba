import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { RouterModule } from '@angular/router';

import { PromocaoRouting } from './promocao.routing';
import { PromocaoComponent } from './promocao.component';
import { PromocaoCriarComponent } from './promocao-criar/promocao-criar.component';

@NgModule({
    declarations: [
        PromocaoComponent,
        PromocaoCriarComponent
    ],
    imports: [
        // Angular
        HttpModule,
        RouterModule,
        CommonModule,
        ReactiveFormsModule,
        FormsModule,

        // Componente
        PromocaoRouting
    ],
    providers: [
        // Serviços        
    ]
})

export class PromocaoModule { }