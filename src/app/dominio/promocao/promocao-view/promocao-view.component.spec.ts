import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PromocaoViewComponent } from './promocao-view.component';

describe('PromocaoViewComponent', () => {
  let component: PromocaoViewComponent;
  let fixture: ComponentFixture<PromocaoViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PromocaoViewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PromocaoViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
