import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PromocaoComponent } from './promocao.component';
import { PromocaoViewComponent } from './promocao-view/promocao-view.component';
import { PromocaoCriarComponent } from './promocao-criar/promocao-criar.component';

const produtoRoutes: Routes = [
    { path: '', component: PromocaoComponent },
    { path: 'promocao-view', component: PromocaoViewComponent },
    { path: 'promocao-criar', component: PromocaoCriarComponent }
];

@NgModule({
    imports: [RouterModule.forChild(produtoRoutes)],
    exports: [RouterModule]
})

export class PromocaoRouting { }