import { Component, OnInit } from '@angular/core';

import { Produto } from '../produto';
import { ProdutoService } from '../produto.service';
import { CarrinhoService } from '../../carrinho/carrinho.service';

@Component({
  selector: 'produto-usuario',
  templateUrl: './produto-usuario.component.html',
  styleUrls: ['./produto-usuario.component.css']
})
export class ProdutoUsuarioComponent implements OnInit {

    produtos: Produto[];

    constructor(
      private produtoService: ProdutoService,
      private cs: CarrinhoService
    ){}

    ngOnInit() {
        
      this.produtoService.buscarTodos()
      .subscribe(resposta => {
        this.produtos = resposta
      });

    }

    addCarrinho(produto: any) {
      this.cs.addCarrinho(produto);

    }

}
