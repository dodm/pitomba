import { Component, OnInit, EventEmitter, Output } from '@angular/core';
import { CarrinhoService, itemCarrinho } from './carrinho.service';

@Component({
  selector: 'app-carrinho',
  templateUrl: './carrinho.component.html',
  styleUrls: ['./carrinho.component.css']
})
export class CarrinhoComponent implements OnInit {

  itemsCarrinho: itemCarrinho[] = [];
  total = 0;
  @Output() eventCont = new EventEmitter();
  constructor(private cs: CarrinhoService) { }

  ngOnInit() {
    this.garante();
  }

  aumenta(item: itemCarrinho) {
    this.cs.addCarrinho(item.produto); //ta errado
    console.log("add item ++ seila ", this.itemsCarrinho);
    this.garante();
  }

  diminui(item: itemCarrinho) {
    this.cs.removeCarrinho(item);
    this.garante();
  }


  private garante() {
    this.total = 0;
    this.cs.items.forEach(n => this.total += n.total);
    this.itemsCarrinho = this.cs.items;
    this.eventCont.emit(this.cs.totalQuantidade);
  }

}
