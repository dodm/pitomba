import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CarrinhoComponent } from './carrinho.component';
import { ProdutoUsuarioComponent } from '../produto/produto-usuario/produto-usuario.component';

const carrinhoRoutes: Routes = [
    { path: '', component: ProdutoUsuarioComponent },
    { path: 'carrinho', component: CarrinhoComponent}    
    
];

@NgModule({
    imports: [RouterModule.forChild(carrinhoRoutes)],
    exports: [RouterModule]
  })

  export class CarrinhoRouting {}