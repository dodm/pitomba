import { Injectable } from '@angular/core';
import { Produto } from '../produto/produto';


export interface itemCarrinho {
    produto: Produto,
    quantidade: number,
    total: number
}

@Injectable()
export class CarrinhoService {
    constructor() {
        this.item = JSON.parse(localStorage.getItem(this.CHAVE))? JSON.parse(localStorage.getItem(this.CHAVE)): [];
        console.log("aAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA",JSON.parse(localStorage.getItem(this.CHAVE)));
        
     }

    item: itemCarrinho[] = [];
    CHAVE:string = 'carrinho';
    // total: number;

    addCarrinho(produto: Produto) {

        if (this.item.findIndex(obj => obj.produto.id == produto.id) == -1) {
            console.log("adicionando novo produto");
            
            this.item.push({
                produto: produto,
                quantidade: 1,
                total: produto.preco
            });
            this.salvarCarrinho();

        }
        else {
            console.log("adicionando novo produto");
            let prod = this.item.find(obj => obj.produto.id == produto.id);
            prod.quantidade++;
            prod.total = prod.quantidade * prod.produto.preco;
            this.salvarCarrinho();

        }

        // this.salvarCarrinho();
        console.log(this.item);

    }

    get items() {
        return this.item
    }

    removeCarrinho(prod: itemCarrinho) {
        console.log("vc chamou '  removeCarrinho(prod: Produto)'");
        
        if (prod.quantidade > 1) {
            console.log("vc entrou no if");
            
            let coisa = this.item.find(ob => ob.produto === prod.produto);
            coisa.quantidade--;
            coisa.total = coisa.produto.preco * coisa.quantidade;
            console.log(coisa);
            this.salvarCarrinho();

            
        } else if (prod.quantidade == 1) {
            console.log("vc entrou no else");
            
            let index = this.item.findIndex(ob => ob.produto === prod.produto);
            this.item.splice(index, 1);
            this.salvarCarrinho();
        }

    }
    get totalQuantidade(){
        let retorno = this.item.reduce((soma, item) => {return soma + item.quantidade},0);
        console.log("retorno é ",retorno);
        
        return retorno;
    }

    private salvarCarrinho() {
        console.log("não sei:::",this.item);
        
        localStorage.clear();
        localStorage.setItem(this.CHAVE,JSON.stringify(this.item));
    }

}