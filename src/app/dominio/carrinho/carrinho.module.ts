import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { RouterModule } from '@angular/router';

import { CarrinhoRouting } from './carrinho.routing';
import { CarrinhoComponent } from './carrinho.component';
import { ProdutoUsuarioComponent } from '../produto/produto-usuario/produto-usuario.component';
import { ProdutoService } from '../produto/produto.service';


@NgModule({
    declarations: [
        CarrinhoComponent,
        ProdutoUsuarioComponent
    ],
    imports: [
        // Angular
        HttpModule,
        RouterModule,
        CommonModule,
        ReactiveFormsModule,
        FormsModule,

        CarrinhoRouting
    ],
    providers: [
        // Serviços
        ProdutoService
    ]
})

export class CarrinhoModule { }